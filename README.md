# Installer - Netagent and AccessTier

This repo provides instructions and examples for automated deployments of Banyan's Enforcement Components (Netagent and AccessTier) using various orchestration platforms:

1. Ansible
2. Kubernetes
3. AWS CloudFormation

---

## Ansible

To use the examples in the Ansible section you will need `Ansible` installed on your local system.

Then, head on over to the [Ansible section](ansible).

## Kubernetes

To use the example in the Kubernetes section you will need a running `Kubernetes` cluster and a configured command line interface `kubectl` installed on your local system.

Then, head on over to the [Kubernetes section](kubernetes).

## AWS CloudFormation

To use the example in AWS CloudFormation section you will need an `Amazon Web Services (AWS)` account with permissions to launch `EC2` instances, manage `AutoScaling` and set `Security Groups`.

Then, head on over to the [AWS CloudFormation section](cloudformation).